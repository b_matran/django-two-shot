# Generated by Django 4.1.2 on 2022-10-20 19:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0011_remove_expensecategory_number_of_receipts"),
    ]

    operations = [
        migrations.AlterField(
            model_name="expensecategory",
            name="name",
            field=models.CharField(max_length=50, null=True),
        ),
    ]
