from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt

# Register your models here.


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
        "number_of_receipts"
    )


@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    list_display = (
        "name",
    )


@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    list_display = (
        "vendor",
        "id",
    )
